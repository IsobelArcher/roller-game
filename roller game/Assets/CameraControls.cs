﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControls : MonoBehaviour {

    Transform thisTrans;

    public Transform player;

	// Use this for initialization
	void Start () {

        thisTrans = this.transform;
		
	}
	
	// Update is called once per frame
	void LateUpdate () {
        thisTrans.position = player.position;

        thisTrans.Rotate(Vector3.right, Input.GetAxis("Mouse Y"));
        thisTrans.Rotate(Vector3.up, Input.GetAxis("Mouse X"));
	}
}
